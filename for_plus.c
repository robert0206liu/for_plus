/*
 * =====================================================================================
 *
 *       Filename:  for_plus.c
 *
 *    Description:  Test ++i and i++
 *
 *        Version:  1.0
 *        Created:  2015年06月08日 11時00分47秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>

int main() {

    int i=0;

    printf("++i\n");
    for(i=0; i<3; ++i) {
        printf("i=%d\n", i);
    }
    printf("out for loop: i=%d\n\n", i);

    printf("i++\n");
    for(i=0; i<3; i++) {
        printf("i=%d\n", i);
    }
    printf("out for loop: i=%d\n", i);

    return 0;
}
